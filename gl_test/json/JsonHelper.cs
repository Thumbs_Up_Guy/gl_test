﻿using System.Net;


namespace gl_test.json
{
    public static class JsonHelper
    {
        public static string GetJsonString()
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            return new  WebClient().DownloadString("https://jujhar.com/bikes.json");
        }
    }

    
}
