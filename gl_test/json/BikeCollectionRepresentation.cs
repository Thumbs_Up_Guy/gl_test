﻿using Newtonsoft.Json;
using System.Collections.Generic;
using gl_test.json;

namespace gl_test.json
{
    public class BikeCollectionRepresentation
    {
        [JsonProperty("items")]
        public List<BikeRepresentation> BikeCollection { get; set; }
    }

}
