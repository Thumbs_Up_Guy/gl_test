﻿using gl_test.Interface;
using System.Collections.Generic;
using Newtonsoft.Json;


namespace gl_test.json
{
    public class BikeRepresentation: IBike<Image>
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        //this neeeds to be treated with care as we have 2 urls in json test reference and 1 url on web page
        [JsonProperty("image")]
        public Image ImageUrl { get; set; }

        public List<string> Class { get; set; }
    }


    public class Image
    {
        [JsonProperty("thumb")]
        public string thumb { get; set; }

        [JsonProperty("large")]
        public string large { get; set; }
    }

}
