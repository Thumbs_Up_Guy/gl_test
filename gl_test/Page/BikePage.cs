﻿using OpenQA.Selenium;

namespace gl_test.Page
{
    class BikePage
    {
        private IWebDriver _driver;

        public BikePage(IWebDriver driver)
        {
            _driver = driver;
        }

        public IWebElement GetFilters() => _driver.FindElement(By.CssSelector("panel-body.ng-scope"));
    }
}
