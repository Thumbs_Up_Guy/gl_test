﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;

namespace gl_test.CoreWebDriver
{
    public static class SeleniumChromeDriver
    {
        public static IWebDriver Driver
        {
            get { return new ChromeDriver(); }
        }
    }
}
