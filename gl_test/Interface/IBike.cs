﻿using System.Collections.Generic;
using System;

namespace gl_test.Interface
{
    public interface IBike<T> where T : class 
    {
        string Id { get; set; }

        string Name { get; set; }

        string Description { get; set; }

        T ImageUrl { get; set; }

        List<string> Class { get; set; }

    }
}
